#!/bin/bash

file=/opt/mpytdl/mpytdl

if [ -f $file ]; then

	sudo rm -r /opt/mpytdl/
	sudo sed -i "/alias mplayer.*\+/d" /etc/bash.bashrc
	source /etc/bash.bashrc
	echo -e "\e[32;1m::\e[37m Reinicie el emulador de terminal para culminar la desinstalación.\e[0m"

fi

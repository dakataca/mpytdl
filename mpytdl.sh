#!/bin/bash

# Este script permite la reprodución y descarga de audios, videos y playlist, a través del reproductor mplayer, usando la herramienta youtube-dl.
# Dependencias: mediainfo, curl, mplayer y youtube-dl.
# Uso : mplayer URL/PATH


#----------------------------- Declaración de variables---------------------------------------------------------#

# Resolución
RESOLUTION=720
#RESOLUTION=1080

# Obtengo el último parámetro pasado a mplayer.
URL=${!#}

# Todos los parámetros pasados a mplayer.
PARAMETERS=$*

# Tiempo de espera en segundos para validar la conexión de una URL.
#TIMEOUT=3,2
TIMEOUT=6

# Número de parámetros.
NUM_PARAMS=$#

# Parámetros pasados a mplayer exceptuando el último parámetro.
INPUT_PARAMS=$(echo $PARAMETERS | awk '!($NF="")')

# Servidor de audio en orden preferente para mplayer.
AUDIO_SERVER="-ao pulse,alsa,jack"
#AUDIO_SERVER="-ao alsa,jack"

# Driver de video.
VIDEO_DRIVER=xv
#VIDEO_DRIVER="vdpau"

# Parámetros de solo video para mplayer. 
ONLY_VIDEO_PARAMS="-fs -progbar-align 98 -vfm ffmpeg -vo $VIDEO_DRIVER -lavdopts threads=$(nproc):fast=1 -framedrop -vf pp=de -alang la,es -slang la,es -ass -embeddedfonts -subcp utf8"
#ONLY_VIDEO_PARAMS="-fs -progbar-align 98 -vfm ffmpeg -vo $VIDEO_DRIVER -lavdopts threads=$(nproc):fast=2:skiploopfilter=nonkey -framedrop -vf pp=de -slang la,es -ass -embeddedfonts"

# Parámetros de solo audio para mplayer.
ONLY_AUDIO_PARAMS="$AUDIO_SERVER -afm ffmpeg"

# Parámetros de video con audio para mplayer.
VIDEO_PARAMS="$ONLY_VIDEO_PARAMS $ONLY_AUDIO_PARAMS -alang la,es -autosync 2"

#----------------------------- Funsiones -----------------------------------------------------------------------#

# Imprimo mensaje del tipo de servicio.
announce() {

	echo -e "\n\e[34;1m==>\e[37m $1 $2...\e[0m"

}


# Establesco formato para youtube-dl.
setytdlformat() {

	## Características del audio.

	# abr: Bitrate de audio.
	# asr: Frecuencia de muestreo de audio.

	onlyaudio="abr<=?320,asr<=?48000"

	## Características del video.

	# witdh: Ancho.
	# height: Alto.
	# vbr: Bitrate de video.
	# abr: Bitrate de audio.
	# fps: Fotogramas por segundo.
	# asr: Frecuencia de muestreo de audio.
	# ?: Valida formatos con valor desconodido.

	if [ $RESOLUTION -eq 720 ]; then		

		RES="HD 30fps"	

		#Formato menor o igual a HD (1280x720p) y a 30 fps o menos.
		#onlyvideo="witdh<=?1280,height<=?800,vbr<=?1650,fps<=?30"
		#onlyvideo="height<=?800"
		onlyvideo="ext=mp4,height<=800"


	elif [ $RESOLUTION -eq 1080 ]; then

		RES="FHD 60fps"	

		#Formato menor o igual a Full HD (1920x1080p) y a 60 fps o menos.
		#onlyvideo="witdh<=?1920,height<=?1080,vbr<=?3810,fps<=?60"
		#onlyvideo="height<=?1080"
		#onlyvideo="height<=?1080"
		#onlyvideo="height<=?1090,fps<=?60,vcodec=h264"
		#onlyvideo="height<=?1090,vcodec=h264"
		onlyvideo="ext=mp4,height<=2000"
	fi

}


# Establecer parámetros y formato general para las demás plataformas.
setgeneralparamsformat() {

	if [[ `echo $PARAMETERS | grep -ow "\-vo null\|\-novideo"` ]]; then

		SERVICE_TYPE="Audio"

		# Establezco parámetros y formato de extracción de solo audio para youtube-dl.
		YTDL_PARAMS="-x -f"
		YTDL_FORMAT="\'bestaudio[$onlyaudio]\'"

		# Establezco parámetros de solo audio para mplayer.
		DEFAULT_PARAMS_MPLAYER=$ONLY_AUDIO_PARAMS

	elif [[ `echo $PARAMETERS | grep -ow "\-ao null"` ]]; then

		SERVICE_TYPE="Video only"

		# Establezco parámetros y formato de solo video para youtube-dl.
		YTDL_PARAMS="-f"
		YTDL_FORMAT="\'bestvideo[$onlyvideo]\'"

		# Establezco parámetros de solo video para mplayer.
		DEFAULT_PARAMS_MPLAYER=$ONLY_VIDEO_PARAMS
	else
		SERVICE_TYPE="Video"

		# Establezco parámetros y formato de video con audio para youtube-dl.
		YTDL_PARAMS="-f"	

		if [[ `echo $PARAMETERS | grep -ow "\-dw"` ]]; then 

			#YTDL_FORMAT="(mp4)best[$onlyvideo,$onlyaudio]"
                      	#YTDL_FORMAT="\'bestvideo[$onlyvideo]+bestaudio/best[$onlyvideo]\'"
                      	#YTDL_FORMAT="\'best[$onlyvideo,$onlyaudio]\'"
                      	YTDL_FORMAT="\'bestvideo[$onlyvideo]+bestaudio[ext=m4a]/bestvideo+bestaudio\'"
			#bestvideo[ext=mp4,height<=720]+bestaudio[ext=m4a]/bestvideo+bestaudio
              	else
			# Streaming solo permite hasta HD.
                      	#YTDL_FORMAT="(mp4)[$onlyvideo,$onlyaudio]"
                      	#YTDL_FORMAT="(mp4)[$onlyvideo]+bestaudio/best"
                      	YTDL_FORMAT="(mp4)[$onlyvideo]"

			# Opción para reproducir video con audio streaming a 1920x1080.
			#YTDL_FORMAT="\'bestvideo[$onlyvideo]\'"
			RES="HD 30fps"	
               	fi
		# (mp4): Solo contenedor mp4.
		#YTDL_FORMAT="(mp4)best[$onlyvideo,$onlyaudio]"

		# Establezco parámetros de video con audio para mplayer.
		DEFAULT_PARAMS_MPLAYER=$VIDEO_PARAMS
	fi

}


# Validar y establecer en orden preferente, el formato para las plataformas más usadas. 
favoriteplatforms() {

	# Establezco parámetros para youtube-dl.
	YTDL_PARAMS="-f"

	# Establezco parámetros de video con audio para mplayer.
	DEFAULT_PARAMS_MPLAYER=$VIDEO_PARAMS

	if [[ `echo $URL | grep -ow "youtube.com\|youtu.be"` ]]; then

		setgeneralparamsformat
		PLATFORM="\e#6\e[97;1;40mYou\e[97;1;41mTube \e[1;40m $RES\e[0m"

	elif [[ `echo $URL | grep -ow "https://gounlimited.to"` ]]; then

		SERVICE_TYPE="Cinecalidad"
		YTDL_FORMAT="sd"
		PLATFORM="\e[38;5;16;48;5;15;1m Cine\e[0m\e[38;5;43;48;5;234;1mcalidad \e[38;5;19;48;5;152;1m GoUnlimited \e[0m"

	elif [[ `echo $URL | grep -ow "https://soundcloud.com"` ]]; then

		setgeneralparamsformat
		PLATFORM="\e[38;5;15;48;5;166;1m SOUNDCLOUD \e[0m"
		SERVICE_TYPE="Souncloud"
		YTDL_FORMAT="\'bestaudio[$onlyaudio]\'"

	elif [[ `echo $URL | grep -ow "o0-[12].com"` ]]; then

		SERVICE_TYPE="Fembed"
		YTDL_FORMAT="mp4"
		PLATFORM="\e[38;5;234;48;5;32;1m All!\e[0m\e[38;5;15;48;5;234;1mcalidad.la \e[0m\e[38;5;41;48;5;236;1m Fembed \e[0m"

	elif [[ `echo $URL | grep -ow "vimeo.com"` ]]; then

		SERVICE_TYPE="Vimeo"
		YTDL_FORMAT="http-${RESOLUTION}p"
		PLATFORM="Vimeo $RES"
		PLATFORM="\e[38;5;15;48;5;39;1m Vimeo  $RES \e[0m"

	elif [[ `echo $URL | grep -ow "xvideos.com"` ]]; then

		SERVICE_TYPE="Xvideos"
		YTDL_FORMAT="mp4-high"
		PLATFORM="\e#6\e[91;1;40mX\e[97mVIDEOS HD\e[0m"

	elif [[ `echo $URL | grep -ow "es.pornhub.com"` ]]; then

		SERVICE_TYPE="PornHub"

		if [ $RESOLUTION -eq 720 ]; then

			YTDL_FORMAT="480p"

		elif [ $RESOLUTION -eq 1080 ]; then

			YTDL_FORMAT="720p"
		fi

		PLATFORM="\e#6\e[38;5;15;48;5;232;1mPorn\e[0m\e[38;5;232;48;5;208;1mhub\e[0m"
	else
		# Establezco parámetros y formato general para las demás plataformas.
		setgeneralparamsformat	

		PLATFORM='\e#6\e[34;109;1mMP!\e[0m\e[30;107;3mytdl\e[0m'
 	fi

}


streaming() {

	#Streaming. 
	announce $SERVICE_TYPE "Streaming"
	echo -e "\e#6\e[5m\u266b\e[0m"

	if [ $MEDIA = "file" ]; then

		if [ "$SERVICE_TYPE" = "Audio" ]; then

			youtube-dl -f $YTDL_FORMAT -o - "$URL" | mplayer -nolirc ${INPUT_PARAMS} $DEFAULT_PARAMS_MPLAYER -cache 30200 -cache-min 5 -
		else
			mplayer -nolirc ${INPUT_PARAMS} $DEFAULT_PARAMS_MPLAYER -cookies -cookies-file /tmp/cookies $(youtube-dl -gf $YTDL_FORMAT --cookies /tmp/cookies $URL)

			# Reproduce video streaming a 1920x1080 con audio, a costa de un exagerado consumo de recursos, no recomendado.
			#mplayer -nolirc ${INPUT_PARAMS} $DEFAULT_PARAMS_MPLAYER -cookies -cookies-file /tmp/cookies $(youtube-dl -gf $YTDL_FORMAT --cookies /tmp/cookies $URL) -audiofile $(youtube-dl -gf $YTDL_FORMAT_AUDIO --cookies /tmp/cookies $URL)
		fi
	else
		# Establezco parámetros de solo audio para mplayer.
		DEFAULT_PARAMS_MPLAYER=$ONLY_AUDIO_PARAMS

		# Reproducción de radio streaming.
		mplayer -nolirc $DEFAULT_PARAMS_MPLAYER $URL
	fi
}


download() {

	if [ $MEDIA = "file" ]; then

		# Si va a descargar una lista de reproducción de youtube. 
		if [[ `echo $URL | grep -ow "playlist?list"` ]]; then

			announce $SERVICE_TYPE "playlist download"
			
			if [ "$SERVICE_TYPE" = "Audio" ]; then

				youtube-dl --audio-format mp3 $YTDL_PARAMS $YTDL_FORMAT -o "$1%(playlist)s/%(playlist_index)s - %(title)s.%(ext)s" $URL 
			else
				youtube-dl $YTDL_PARAMS $YTDL_FORMAT -o "$1%(playlist)s/%(playlist_index)s - %(title)s.%(ext)s" $URL 
			fi
		else
			announce $SERVICE_TYPE "download"

			if [ "$SERVICE_TYPE" = "Audio" ]; then

				youtube-dl --audio-format mp3  $YTDL_PARAMS $YTDL_FORMAT -o "$1%(title)s.%(ext)s" $URL 
			else
				youtube-dl $YTDL_PARAMS $YTDL_FORMAT -o "$1%(title)s.%(ext)s" $URL 
			fi
		fi
	else
		echo -e "\e[31;1mError\e[0m\e[1m: No es posible descargar $MEDIA. \e[0m"
	fi

}


# Descargar o hacer streaming de video(s)/audio(s) usando youtube-dl.
getmedia() {

	if [[ $MEDIA = "file" ]]; then

		# Imprimo tipo de servicio online y plataforma.
		echo -e "\e#6\e[34;107;1m MP\e[0m\e[30;107;3mlayer \e[0m\e[37;100m Online \e[0m \n\n$PLATFORM"
	
	elif [[ `echo $PARAMETERS | grep -ow "\-dw"` != "-dw" ]]; then 
		
		# Imprimo tipo de servicio online y plataforma.
		echo -e "\e#6\e[34;107;1m MP\e[0m\e[30;107;3mlayer \e[0m\e[37;100m Online \e[0m \n\n$PLATFORM"

	fi

	# Si uno de los parámetros es -dw, habilite la descarga del fichero multimedia.
	if [[ `echo $PARAMETERS | grep -ow "\-dw"` ]]; then 

		# Si el número de parámetros es mayor o igual a 3.
		if [ $NUM_PARAMS -ge 3 ]; then

			# Obtengo el parámetro seguido de -dw. 
			SAVE_PATH=$(echo "$INPUT_PARAMS" | grep -ow "\-dw .*" | cut -d ' ' -f2)

			# Si el parámetro seguido de -dw es una ruta existente.
			if [[ -d $SAVE_PATH ]]; then

				download $SAVE_PATH	
			else
				# Descargo fichero multimedia en la ruta actual.
				download ""	
			fi

		else
			# Descargo fichero multimedia en la ruta actual.
			download "" 
		fi

	else
		streaming
	fi

}


#----------------------------- Main ------------------------------------------------------#


# Si es un fichero y existe. (las comilllas son necesarias para validar cuando es un fichero local con espacios)
if [ -f "$URL" ]; then

	# Local

	# Obtengo los metadatos del fichero y filtro información de Audio y Video.
	METADATA=$(mediainfo "$URL" | grep -ow "Video\|Audio" | uniq)

	# Si los metadatos no muestran información de video ni audio.
	if [[ $METADATA = "" ]]; then

		# valido si es una lista m3u.
		if [[ `echo $URL | grep -o "\.m3u8\?"` ]]; then

			# Video con audio local.
			FILE="lista"

			# Establezco parámetros de video con audio para mplayer en local.
			DEFAULT_PARAMS_MPLAYER=$VIDEO_PARAMS
		else
			# Fichero no es video, audio o lista m3u.
			FILE=""
		fi

	# Si en la primera línea indica video.
	elif [[ `echo $METADATA | head -n 1` =~ "Video" ]]; then

		# Si en la segunda línea indica que es un video con audio.
		if [[ `echo $METADATA | head -n 2` =~ "Audio" ]]; then

			# Video con audio local.
			FILE="video"

			# Establezco parámetros de video con audio para mplayer en local.
			DEFAULT_PARAMS_MPLAYER=$VIDEO_PARAMS
		else
			# Video con audio local.
			FILE="only video"

			# Establezco parámetros de solo video para mplayer en local.
			DEFAULT_PARAMS_MPLAYER=$ONLY_VIDEO_PARAMS
		fi
	else
		# Es audio solo.
		FILE="audio"

		# Establezco parámetros de solo audio para mplayer en local.
		DEFAULT_PARAMS_MPLAYER="$ONLY_AUDIO_PARAMS -shuffle"
	fi

	if [[ $FILE != "" ]]; then

		echo -e "\e#6\e[34;107;1m MP\e[0m\e[30;107;3mlayer \e[0m"
		announce "Playing" "$FILE"

		# Establesco parámetros por defecto y de entrada, incluyendo el fichero de entrada para reproducción local.
 		mplayer -nolirc $DEFAULT_PARAMS_MPLAYER "$@"
	else
		echo -e "\e[31;1mError\e[0m\e[1m: Fichero no es video ni audio. \e[0m"
	fi

elif [[ `echo $URL | egrep -ow "https?://(www)?.*\..*"` ]]; then

	PULL_REQUEST=$(timeout -s SIGKILL 8 curl -Is --connect-timeout $TIMEOUT $URL | egrep "HTTP/(1.1|2) [0-9]{3}( OK)?")
	#echo "PULL_REQUEST: $PULL_REQUEST"

	if [[  $PULL_REQUEST != "" ]]; then

		#if [[ `echo $PULL_REQUEST | grep -ow 'Bad Request\|Error\|no\-cache\|unavailable\|Connection\|Methods:\|Not'` ]]; then

		# Reproducir un fichero streaming.
		MEDIA="file"

		# Establesco formato para ytdl.
		setytdlformat

		# Valido las plataformas favoritas.
		favoriteplatforms

		# Streaming o descarga de video(s)/audio(s).	
		getmedia

	elif [[ `echo $URL | egrep -o "stream|:[0-9]{4,5}|aac|\.fm/|\.m3u8?|mp3|live|//[0-9]{3,5}|radio|RADIO"` ]]; then
		PLATFORM="\e[38;5;15;48;5;234;1mRadio regex Online \u266A\e[0m"
		SERVICE_TYPE="Radio"
		MEDIA="radio"
		getmedia
	else
		echo -e "\e[33;1mWarning\e[0m\e[1m: Tiempo de espera agotado! \e[0m"
	fi

elif [[ `echo $PARAMETERS | egrep -ow "(-){,2}h(elp)?"` ]]; then

	# Opciones de ayuda de mplayer.	
	mplayer "$@"

# Si el último parámetro es un fichero.
elif [[ `echo $URL | egrep -ow ".*\.[a-z0-9]+"` ]]; then

	echo -e "\e[31;1mError\e[0m\e[1m: Fichero multimedia inválido. \e[0m"
else			
	echo -e "\e[31;1mError de capa 8\e[0m\e[1m: La causa está entre el teclado, el mouse y la silla! \e[0m"
fi

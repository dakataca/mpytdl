#!/bin/bash

file=/opt/mpytdl/mpytdl

sudo mkdir -p /opt/mpytdl 
sudo cp -u mpytdl.sh $file 
sudo chmod a+x $file


# líneas a insertar.
declare -a bashrc=(
'alias mplayer="/opt/mpytdl/mpytdl"'
)

source insertlines.sh "${bashrc[@]}" /etc/bash.bashrc
source /etc/bash.bashrc

# Insertar resolución preferida entre HD y FHD.
insert_resolution() {

	if [[ $1 -eq 1 ]]; then

		if [[ `grep "RESOLUTION=720" $file` ]]; then

			echo "Resolución previamente establecida en 720p."
		else
			sudo sed -i "s/RESOLUTION=1080/RESOLUTION=720/" $file 
			echo "Resolución HD 1080x720p establecida."
		fi
	else
		if [[ `grep "RESOLUTION=1080" $file` ]]; then

			echo "Resolución previamente establecida en 1080p."
		else
			sudo sed -i "s/RESOLUTION=720/RESOLUTION=1080/" $file 
			echo "Resolución FHD 1920x1080p establecida."
		fi
	fi


}

PS3="Seleccione resolución de video a descargar: "
select var in "HD 1280x720" "FHD 1920x1080"
do
	case $REPLY in
		1) insert_resolution 1
		echo -e "\e[32;1m::\e[37m Reinicie su emulador de terminal para culminar la instalación.\e[0m"
		break;;

		2) insert_resolution 2
		echo -e "\e[32;1m::\e[37m Reinicie su emulador de terminal para culminar la instalación.\e[0m"
		break;;

		*) echo "Opción inválida."
		;;
	esac
done



![alt text](img/mpytdl.jpeg "Title Text")


# mpytdl.sh
Es un script escrito en `bash` que añade la funcionalidad de reproducción **streaming HD** y **descarga** de *audios*, *videos* y *playlist* en **HD** y **Full HD**, al reproductor [mplayer](https://www.archlinux.org/packages/extra/x86_64/mplayer/) con videos de las [plataformas compatibles](http://ytdl-org.github.io/youtube-dl/supportedsites.html) con [youtube-dl](https://www.archlinux.org/packages/community/any/youtube-dl/) y algunas otras.

## Plataformas validadas
Lista de las plataformas compatibles que hasta ahora han sido validadas.
- _YouTube._
- _Facebook._
- _Soundcloud_
- _Instagram._
- _Vimeo._
- _Cinecalidad(Gounlimited)._
- _Allcalidad(Fembed)._
- _Platzi._
- _Xvideos._
- _PornHub._


## Pre-requisitos
- _Instalar el visualizador de metadatos [mediainfo](https://www.archlinux.org/packages/community/x86_64/mediainfo/), la herramienta [curl](https://www.archlinux.org/packages/core/x86_64/curl/), el reproductor [mplayer](https://www.archlinux.org/packages/extra/x86_64/mplayer/) y la herramienta [youtube-dl](https://archive.archlinux.org/packages/y/youtube-dl/)._

### Intel
Están oficialmente soportados los drivers de video con soporte 3D [modesetting](https://jlk.fjfi.cvut.cz/arch/manpages/man/extra/xorg-server/modesetting.4.en) y [xf86-video-intel](https://jlk.fjfi.cvut.cz/arch/manpages/man/extra/xf86-video-intel/intel.4.en).

### AMD
Sin probar.

## Configuración predeterminada
Para efectos de estadarización, se ha preestablecido una configuración básica de `mplayer` para reproducción *local* y *remota* en el script, como se describe acontinuación:

### Configuración predeterminada para reproducción de video.

- _Deshabilitar [LIRC](https://es.wikipedia.org/wiki/LIRC)._
- _Reproducción a pantalla completa._
- _Ubique la barra de volumen en la parte inferior_.
- _Priorice la familia de codecs de audio y video `ffmpeg`._
- _Extensión XVideo de XFree86 4.x para habilitar la reproducción acelerada por hardware._
- _Utilice todos los núcleos lógicos del equipo._
- _Optimización en la reproducción de videos, cuando el hardware no cumpla con las especificaciones del video (útil en equipos lentos)._
- _Salte fotogramas cuando reproducción no sea fluida (útil en equipos lentos)._
- _Use el servidor de audio `pulse`, si no está activo use `alsa`, de lo contrario use `jack`_.
- _Habilite de forma suave la [sincronización de audio y video](https://es.wikipedia.org/wiki/Sincronizaci%C3%B3n_de_audio_y_video)._
- _Habilite filtros de postprocesamiento por defecto._
- _Establezca el audio y los subtítulos (si existen) en idioma latino, si no existe use español, de lo contrario use el predeterminado._
- _Habilita el correcto funcionamiento de los subtítulos formateados en Advanced SubStation Alpha (ass) o SubStation Alpha (ssa)._
- _Establezca el formato de codificación de caracteres `utf8` para los subtítulos (si existen)._
```shell
$ mplayer -nolirc -fs -progbar-align 98 -afm ffmpeg -vfm ffmpeg -vo xv -lavdopts threads=$(nproc):fast=1 -framedrop -ao pulse,alsa,jack -autosync 2 -vf pp=de -alang la,es -slang la,es -ass -embeddedfonts -subcp utf8 /path/to/video.mkv
```
### Configuración predeterminada para reproducción de solo audio 

- _Deshabilitar [LIRC](https://es.wikipedia.org/wiki/LIRC)._
- _Use el servidor de audio `pulse`, si no está activo use `alsa`, de lo contrario use `jack`_.
- _Priorice la familia de codecs de audio `ffmpeg`._
```shell
$ mplayer -nolirc -ao pulse,alsa,jack -afm ffmpeg /path/to/file.m4a
```

### Configuración predeterminada para reproducción de solo video

- _Deshabilitar [LIRC](https://es.wikipedia.org/wiki/LIRC)._
- _Reproducción a pantalla completa._
- _Priorice la familia de codecs de video `ffmpeg`._
- _Extensión XVideo de XFree86 4.x para habilitar la reproducción acelerada por hardware._
- _Utilice todos los núcleos lógicos del equipo._
- _Optimización en la reproducción de videos, cuando el hardware no cumpla con las especificaciones del video (útil en equipos lentos)._
- _Salte fotogramas cuando reproducción no sea fluida (útil en equipos lentos)._
- _Habilite filtros de postprocesamiento por defecto._
- _Establezca subtítulos (si existen) en idioma latino, si no existe use español, de lo contrario use inglés._
- _Habilita el correcto funcionamiento de los subtítulos formateados en Advanced SubStation Alpha (ass) o SubStation Alpha (ssa)._
- _Establezca el formato de codificación de caracteres `utf8` para los subtítulos (si existen)._

```shell
$ mplayer -nolirc -fs -vfm ffmpeg -vo xv -lavdopts threads=$(nproc):fast=1 -framedrop -vf pp=de -slang la,es,en -ass -embeddedfonts -subcp utf8 /path/to/video.mkv
```

_Si ha establecido alguno de los anteriores parámetros en el [fichero de configuración](https://jlk.fjfi.cvut.cz/arch/manpages/man/extra/mplayer/mplayer.1.es#FICHEROS_DE_CONFIGURACI__u00D3_N) de `mplayer`, deberá quitarlos del [fichero de configuración](https://jlk.fjfi.cvut.cz/arch/manpages/man/extra/mplayer/mplayer.1.es#FICHEROS_DE_CONFIGURACI__u00D3_N) (recomendado) o del script manualmente. Si no sabe lo que hace mejor no toque el script, puede afectar su correcto funcionamiento.
Si añade alguno de los anteriores parámetros en la invocación de `mplayer` o en el [fichero de configuración](https://jlk.fjfi.cvut.cz/arch/manpages/man/extra/mplayer/mplayer.1.es#FICHEROS_DE_CONFIGURACI__u00D3_N) no afectará su funcionamiento, pero se recomienda no hacerlo._

## Reproducción en local
Para reproducir un fichero de *audio* o *video* que está en su equipo, tan solo ponga `mplayer` y como último parámetro la *ruta* del fichero. Cualquier otro parámetro adicional distinto a los preestablecidos en el *script* y en el [fichero de configuración](https://jlk.fjfi.cvut.cz/arch/manpages/man/extra/mplayer/mplayer.1.es#FICHEROS_DE_CONFIGURACI__u00D3_N), deberá ser añadido antes de la *ruta* del fichero.
```shell
$ mplayer /path/to/file.mkv
```

## Instalación

Clone el repositorio.
```shell
$ git clone https://gitlab.com/dakataca/mpytdl.git
```
Ingrece al directorio del repositorio.
```shell
$ cd mpytdl/
```
Liste el contenido del repositorio.
```shell
$ ls
```
Ejecute el script de instalación.
```shell
$ source install.sh 
```

## Desinstalación

Clone el repositorio.
```shell
$ git clone https://gitlab.com/dakataca/mpytdl.git
```
Ingrece al directorio del repositorio.
```shell
$ cd mpytdl/
```
Liste el contenido del repositorio.
```shell
$ ls
```
Ejecute el script de desinstalación.
```shell
$ source uninstall.sh 
```

## Reproducción online 

### Videos

#### Streaming de video/playlist
Si desea reproducir un **video/playlist** streaming con `mplayer` tan solo añada como último parámetro la *URL*.
```shell
$ mplayer <URL>
```

#### Descarga de video/playlist
Si desea descargar un **video/playlist** con `mplayer` tan solo agregue la opción `-dw` para habilitar la descarga, y añada como último parámetro la *URL*.
```shell
$ mplayer -dw <URL>
```
#### Descarga de video/playlist en una ruta específica
Si desea descargar un **video/playlist** con `mplayer` en una ruta específica, tan solo agregue la opción `-dw` para habilitar la descarga, seguido de la ruta, y añada como último parámetro la *URL*.
```shell
$ mplayer -dw ~/Vídeos/ <URL>
```

#### Streaming de solo video/playlist (sin audio)
Si desea reproducir un **video/playlist** sin audio con `mplayer` tan solo agregue la opción `-ao null` para solo video, y añada como último parámetro la *URL*.
```shell
$ mplayer -ao null <URL>
```
La descarga de *playlist de youtube* creará un directorio con el nombre de la lista de reproducción, y dentro descargará todos los videos de la *playlist*.

#### Descarga de solo video/playlist (sin audio) en una ruta específica
Si desea descargar un **video/playlist** sin audio con `mplayer` en una ruta específica, tan solo agregue `-ao null` para solo video, `-dw` para habilitar la descarga, seguido de la ruta, y añada como último parámetro la *URL*.
```shell
$ mplayer -ao null -dw ~/Vídeos/ <URL>
```

### Audios

#### Streaming de audio
Si desea reproducir audio streaming de **video/playlist** con `mplayer` tan solo añada `-vo null` o `-novideo`, y como último parámetro la *URL*.
```shell
$ mplayer -vo null <URL>
```
ó
```shell
$ mplayer -novideo <URL>
```

#### Descarga de audio
Si desea descargar audio con `mplayer` tan solo añada `-vo null` o `-novideo` para solo audio, el parámetro `-dw` para habilitar la descarga, y como último parámetro la *URL*.
```shell
$ mplayer -vo null -dw <URL>
```
ó
```shell
$ mplayer -novideo -dw <URL>
```

#### Descarga de audio en una ruta específica
Si desea descargar audio con `mplayer` en una ruta específica tan solo añada `-vo null` o `-novideo` para solo audio, el parámetro `-dw` para habilitar la descarga, seguido de la ruta, y como último parámetro la *URL*.
```shell
$ mplayer -vo null -dw ~/Música/ <URL>
```
ó
```shell
$ mplayer -novideo -dw ~/Música/ <URL>
```

### Radio

Si desea reproducir *radio online* tan solo ponga `mplayer`, seguido de la *URL* de la radio streaming. Por ejemplo para reproducir la emisora [La Kalle](https://lakalle.bluradio.com/) de [Bogotá/Colombia](https://es.wikipedia.org/wiki/Bogot%C3%A1).
```shell
mplayer http://19473.live.streamtheworld.com:3690/LA_KALLE_SC
```
